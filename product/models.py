from django.db import models

# Create your models here.


class Product(models.Model):
    CATEGORY = (
        ('Men','Men'),
        ('Women','Women'),
        ('All','All'),
    )
    name = models.CharField(max_length=250, null=True)
    brand = models.CharField(max_length=250, null=True)
    price = models.FloatField(null=True)
    category = models.CharField(max_length=250, null=True, choices=CATEGORY)
    description = models.TextField(null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.name